# Personal Notes
Linux Basic commands 

1.ls-list directory
    ls -A : showing almost all list of files from current working directory
    ls  -a: showing  all list of files from current working directory  Like entries starting with .
    ls  -c: showing time of last modification of file status information 
    ls -C: showing list entries by columns
    ls -lh: showing list of files with human readable format like size of file
    ls  -i: showing index number of each file
    ls -Q: enclose entry names in double quotation like “hi”
    ls -s -r: reverse order while sorting like size of file 
    ls -s : print allocated file size
    ls -S:sort by file size,largest first
    ls -U: don’t sort ,list entries in directory order 
    ls -x: list entries by lines instead of columns
    ls -Z: list any security context of each file
2.cd-change directory

3.pwd-print working directory

4.head - text file handling
head -n( 5 or 10 ,32) :to print number of lines
head -q : it will never print header of file broken lines
head -v :it will print header of file upto broken lines   
head -z: it will print all lines of file upto delimiter is null,not new lines

5.git-the stupid content tracker
    git clone path:collect data from git account
    git version: will print current git version
        git rm ‘file name ’:To remove file from git(repositories)
    git  add *: file changes added to git
    git commit -m “Your messages”:file changes mentioning by short message
    git push origin <branch Name>:To push changes to git & mention branch also like master          
    or sub branch
    git checkout -b “Mohan”: To create new sub branch 
    git branch:To check branch’s name and which you were working  
    Git status:To check status of git add or removing file /new changes of git
6.less-its like more

2. File handling   
    fg : foreground process like echo hello 
    Bg :background process like echo hello &
